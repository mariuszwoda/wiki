
# maven

* installing on mac os
    * download maven zip
    * unzip files
    * add the bin directory of the created directory apache-maven-3.6.0 to the PATH environment variable (f.e. /Users/mario/Documents/development/apache-maven-3.6.0/bin:)
    * aftr console restart check `mvn -v` if installed correctly
    
    ```
    ~/Documents/development/workspace $ mvn -v
    Apache Maven 3.6.0 (97c98ec64a1fdfee7767ce5ffb20918da4f719f3; 2018-10-24T20:41:47+02:00)
    Maven home: /Users/mario/Documents/development/apache-maven-3.6.0
    Java version: 1.8.0_77, vendor: Oracle Corporation, runtime: /Library/Java/JavaVirtualMachines/jdk1.8.0_77.jdk/Contents/Home/jre
    Default locale: en_US, platform encoding: UTF-8
    OS name: "mac os x", version: "10.13.6", arch: "x86_64", family: "mac"
    ``` 

# rest

* http://www.where2play.pl/index.php/json/checkInDatabase?callback=JSON_CALLBACK&amp;fun=addEventTest
* http://www.where2play.pl/index.php/json/checkInDatabase?fun=deleteEventTest


# sql

`SELECT * FROM event JOIN user on event.creation_by = user.UUID` todo
