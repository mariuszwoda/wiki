# node.js

## workshop [stacja.it](https://stacja.it/warsztaty/2018-12-15-Full-Stack-JavaScript-Nowoczesny-Backend-w-Node.js.html), 2018-12-15
Prowadzenie: Piotr Błaszczak

### teoria
- jednowątkowy, event loop
- package-lock.json - snapshot wersji konkretnych ktory uzywamy np po dlugim czasie zeby nie bylo konfliktow wersji
`tslint.json` sprawdza kod, sredniki, spacja itp; przed mergem 
`tsconfig.json` 
`package.json`: `scripts` - komendy przydatne po czasie  np dla build: `npn run build` 
`vscode icons` - dla Visual Studio Code
`visual studio intellicode`
`npm i -g typescript` trzeba zainstalowac globalnie np potrzebne przy kontrolerach
`npm i -g typescript@latest`

### praktyka

[repo](https://bitbucket.org/myflowpl/nestjs-project/src/master/)
[zadania](https://myflowpl.bitbucket.io/nestjs/_zadania/index.html)

`npm install` tworzy projekt?
`npm run test` uruchamia testy; wczesniej trzeba `npm install`
`npm run build` buduje paczke, tworzy sie paczka node_modules
`npm run start` startuje projekt na localhost:3000

`nest generate module user`  generuje user i app
`nest g mo user`
`node dist/main.js`
`nest g controller user/controller`
`http://localhost:3000/user/stats`

* testy
`test:e2e` - komend sa w package.json; korzysta z biblioteki jest (lepsza od spock np ;)
`npm run test:e2e:one test/user.e2e-spec.ts` uruchomienie jednego testu

`models.ts` musimy dodac `export` aby byl widoczny w projekcie

`create barrel` plugin `NG.42 TS Helpers` reeksportuje wszyskie pliki np w `dto/index.ts`

jesli funkcja jest `async` to mozemy uzyc `await` i musi zwracac `Promise<T>`
	
`npm i jsonwebtoken` do prawdziwego logowania userow

`nest g service user/service/user --flat` tworzy serwis usera; katalog i pliki ts

`npm i @types/jsonwebtoken --save-dev` zeby moc uzywac jsonwebtoken w UserService

`npm i @nestjs/swagger` instalacja swaggera (dla kontraktu frontend <-> backend); po zainstalowanieu VisualStudio moze nie widziec klas z nowego pakietu nalezy odwiezyc ew dopisac import recznie! `import * from '@nestjs/swagger'` -> `localhost:3000/docs/`

* swagger

`@ApiModelProperty`, `@ApiModelPropertyOptional` dekoratory dla swaggera
`@ApiResponse...` co dana metoda bedzie zwracac
`swagger-api -> swagger-codegen` 


### Skroty: konsola Visual Studio Code
`cmd+shift+p` konsola
`ctrl+p` przemieszacznie miedzy plikami
`shift+f12` klasy gdzie jest uzyta metoda
`alt+shift+f` formatowanie


### przygotowanie srodowiska

Cześć,

przypominamy, że widzimy się już w najbliższą sobotę o godz. 9:00 w Sages przy ul. Nowogrodzkiej 62c w Warszawie (domofon 48, piętro II) na warsztatach FULL STACK JAVASCRIPT - NOWOCZESNY BACKEND W NODE.JS!

INSTRUKCJA PRZYGOTOWANIA ŚRODOWISKA

W celu przygotowania komputera do warsztatu, należy zainstalować:

1) Node.js w wersji 10.x.x 
do pobrania ze strony [node](https://nodejs.org) 
można użyć też wersji 11.x.x

This package has installed:
	•	Node.js v11.4.0 to /usr/local/bin/node
	•	npm v6.4.1 to /usr/local/bin/npm
Make sure that /usr/local/bin is in your $PATH.

Żeby zweryfikować poprawność instalacji, uruchamiamy nową konsolę i wpisujemy dwie komendy

`node --version`

`npm --version`

a komendy powinny wyświetlić zainstalowane wersje, np:

`node -> 10.14.1`

`npm -> 6.4.1`

2)  @nestjs/cli
otwieramy konsolę i używamy komendy:

`npm install -g @nestjs/cli@latest`

żeby zweryfikować poprawność instalacji, tworzymy nowy projekt:
w konsoli, w katalogu domowym używamy komendy:

`nest new nodejs-api`

zostaniemy zapytani o: description, version i author - możemy te opcje pominąć klikając enter
jeśli zostaniemy zapytani Which package manager would you ❤️  to use? - wybieramy npm
po zakończenu instalacji, wpisujemy dwie komendy:

`cd nodejs-api`
`npm run start:dev`

teraz powinnismy na adresie [localhost:3000](http://localhost:3000/) zobaczyć napis Hello World!

3) System kontroli wersji GIT
do pobrania ze strony: [git](https://git-scm.com/downloads); 
podczas instalacji wybieramy domyślne opcje

4) Edytor Visual Studio Code
do pobrania za darmo ze strony: [visualstudio](https://code.visualstudio.com/);
jeśli używasz innego edytora, upewnij się że wspiera on składnię TypeScript

w razie problemów z instalacją zapraszam na naszego Slack'a.

KANAŁ NA SLACKU

Właśnie otrzymałeś od nas zaproszenie do kanału na Slacku. Jeśli nie używasz tego narzędzia, prosimy o instalację. Slack to komunikator, na którym trener będzie podczas warsztatu komunikował się z uczestnikami, przesyłał potrzebne pliki.

Jeżeli już kiedyś korzystałeś/aś z naszego Slacka na jakimś warsztacie, wejdź na https://slack.com/signin, wpisz workspace stacjait, zaloguj się i znajdź po prostu kanał o nazwie #fullstackjs_151218. Będziesz do niego automatycznie dodany/a i będziesz miał/a do niego dostęp.

Do zobaczenia! :)

-- 
Pozdrawiam,
Marek Nowacki / Junior Events Coordinator
Sages Sp. z o.o. / www.sages.com.pl
T: 881 680 476  / M: m.nowacki@sages.com.pl
