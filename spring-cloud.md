#spring cloud

## workshop [itstacja](https://stacja.it/podcast/2018-08-28-lukasz-andrzejewski-o-architekturze-mikroserwisow.html) 2019-07-28, 
Prowadzenie: Łukasz Andrzejewski, l.andrzejwski@sages.com.pl

* bardzo fajnie na kazdy z tematow prowadzacy mial swoj lokalny branch ktory pushowal na git 
* uczestnicy pullowali kazdy nowy branch ktory dzialal (lub nie ;))


* IDEA: klikamy na `pom.xml` i `add as maven project`, wowczas mozna uruachmiac 'jako spring projekt'

`http://localhost:8000/sayHello?name=aaa` projekt demo

`http://localhost:8000/swagger-ui.html` projekt departamenty i swagger

**Lombok:** 

`@Data` generuje getery i setery (jesli nadpiszemy to wygrywa nasza wersja)

`@RequiredArgsConstruktor` nie mozna zainicjalizowac nullem nasz departament

`class Mapper` maly adapter, w naszym przypadku bedziemy mapowac get/set

`*.repository` warstwa utrwalania (spring data), cala warstwa utrwalania sprowadza sie do tworzenia interfejsow

* kontroler nie powinien zawierac logiki biznesowej, tylko scala projekt DepartamentService - tylko polaczenie do bazy

`ControllerAdvice` lapie wyjatki, wysylamy je do klienta 

`ExceptionDto` opis wyjatku dla biznesu

`Beans` dla swaggera konfig (dla frontendowca przydatne)

`http://localhost:8000/h2` na czas dev fajna baza

`dependencyManager` spina zaleznosci od springclouda

[http://projects.spring.io/spring-cloud/]() kopiujemy wersje 

`mvn clean` z linii polecen przebudownie projektu

`mvn package`

**Narzedzia:**

`eureka` rejest dzialajacych serwisow

* projekt `departments2` odpalony aby zobaczyc czy w eurece pojawi sie drugi department - rzeczywiscie tak jest, pojawil sie na dwoch roznych portach

`*.starter-config` dobrze uzywac we wszystkich mikroserwisach zeby wurzucic konfig na zewnatrz

`gateway` to loadbalancer, troche router

`zuul` gateway serwer (na poziomie beana `@EnableZuulProxy`)

`ribbon` jako loadbalancer w zuulu mozna uzyc

`06-synch-commun` repo: z poziomu usera moge pobrac rejestr z eureki  (discoveryClient) i pobrac uri+port - zwraca pelen fizyczny adres do end pointu do ktorego bede uderzal

`feign` wykorzystujemy do pobrania danych np (dane z cache albo z bazy zalezy od implementacji cache)

`zipkin` przez jakie nody przechodzilismy; wydajnnosc jakie spadki/czas i gdzie; narzedzie ktore pozwala to sledzic; jaki wyjatek na ktorej instancji polecial

`sleuth` (_wym. sloot_, szpieg) percentage: 1 - to bedziemy powiekszac aby znalezc bledy (np na 100 aby sleuth sprawdzil wszystko)

`hystrix wrapper/aspekt` jesli np nie uda sie zainicjowac czegos to zwrocimy np domyslna wartosc; metoda zapasowa jesli inny serwis nie dziala

`run-kafka.sh` uruchamia kawke na dockerze (najpierw trzeba uzyc dockera)

`kafka` to tez moze byc cachelog np gdy jeden serwis nie dziala; posrednik w komuikacji

`08-as-com` branch: informacje przekazujemy za pomoca kafki

`users->service->emiter` wysylamy do kafki; destinations: new-users
`dep->config->beans` Sink - dostajemy to co wyslalismy do kafki; destinations: new-users

* w ramach grupy komunikat bedzie przetworzony tylko raz

* nowy package: *cloud-stream; *stream-kafka

[www.microservices.io]() opisane wzorce do komunikacji i dobre praktyki


09 branch:
`rest.http`  prymitywny rest w idei

`jwt` jason web token w security

users: klasa User implemntuje jako UserDetails - np getAutoritirs() - customowy obiekt
Autority impl GrantedAutority
loadUserByUsername(String)

w users: duzo ustawien security ale w innych juz tylko kopia tego co jest w departament
w depart mamy dla security tylko w: 
security: JWTToken* i config/ResourceServer

10 docker:

w client: 
`npm install`

Zwby uruchomic -> angular niestartowal (usunelismy klienta angulara)
```
./rebuilt.sh
./docker-rebuild.sh
docker-compose up  
//docker-compose up configuration-serwer 
./docker-stop.sh //usuwa wszystskie kontenery
```

* lepiej uzyc kubernetesa na produkcji
