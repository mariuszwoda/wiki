# spring

## spring batch (udemy course 2019-02-24)

- spring batch at most fundamental is a state machine
- `job` defines the list of states and how to transition from one state to the next (state of steps) 
- job has many steps  
- `step` represents the independent piece of processing that makes up the job 
- there are two differents types of steps: tasklet and chunk  
- `tasklet`: single interface with a single method you can execute
- `chunk`: 3 main components: `ItemReader` (input of the step), `ItemProcesor` (optional additional logic) and `Item Writer` (output of the step)
- state of the job is maintain in a `JobRepository` (default in memory database )

## spring boot

* you need [gradle](https://gradle.org/install/) or [maven](https://maven.apache.org/) to build project

`brew install gradle`

* build project (from project root)

`gradle clean`

`gradle clean -i` show info

`gradle clean -i` show debug

`gradle build` build project and create war/jar in `build/libs/` directory

`java -jar build/libs/file.jar` run jar from console

* it is possible to run project from IDE
`right click on spring class with main method then choose: run as *.java`  


# Workshop spring-boot, [Altkom](https://www.altkomakademia.pl/szkolenia/oferta/katalog-szkolen/i/spring-boot), 23-25.07.2018 
Prowadzacy: Tomasz Popis

Link do logowania [altkom](https://klasa.altkomakademia.pl/wirtualna-klasa/1cb43915-23f4-420b-9c2a-40f64b8072fa)

**Pytania:**
- przydatne pluginy w ideii podczas developmentu; jakiego ide najlepiej uzywaj (najwygodniejszy?)
- gdzie trzymac pliki ze stalymi, czy springb ma cos dedykowanego oprocz np app.properties?
- od czego zaczac przejmujac projekt spring(boot), jakas dokumentacja automatycznie?
//- w commission moze przeniesc konfigi z xmlow do klas konfiguracyjnych?
- jakis przyklad standarowych komend w konsoli dla spring-boot?
- www gdzie znajdziemy wszystkie zrodla do build.gradle
- poprosic o prezentacje
- przykladowy projekt controlerty, serwisy, respoitory i komponenty biznesowe (domena) + klasy konfigurujace
- czy sa takie gotowce do autentykacji/autoryzacji/entity/komponenty/kontrolery/zabezpieczenia/z kolejkami?/ (pewnie lepiej zrobic jeden raz indywidualnie)

## 25.07

**spring security:**
- [owasp](https://www.owasp.org/index.php/Top_10-2017_Top_10) - top 10 bledow w roku; przydatne np na code review
- `KaliLinux (Debian)` do testow security
- zeby skonfigrowac trzeba zrobic co najmniej jedna klase konfiguracyjna security

**zabezpieczenia:**
- konfig uzytkownikow i roli
- konfiguracje bezpieczenstwa na koncowkach url/rest (metoda configure(HttpSecurity))
- filtrujemy i sprawdzamy naglowki http
- zabezpieczenia na metodach biznesowych (lub resources): 
`@Secured` (bez "OR" zawsze "AND"),  `@PreAutorize/PostAutorize` (z "OR")
- role wylistowane jako string np "R1, R2, R3" lub "R1 R2 R3"

- `secureapp projekt` autoryzacja przez api token
- jest tez adnotacja ktora zrobi autentykacje springowa zamiast po drodze projekt secureapp (np projekt Oauth)

moda aby pisac backednd w node.js ;)	

**spring cloud**
- sc to poprostu netflix
- `crud` create read update delete

wzorce projektowe: proksy, fasada, gateway

`EPI messaging` kolejki ..

`bootstrap.prop` nadpisuje jakby `app.prop`

`cocoaRestClient` dla maca

`Eureka` rozdziela duplikuje mikroserwisy (z mesosem?)

`ApacheMesos` do orchiestracji (jenkins buduje powiadamia mesosa, mesos uruchamia microserwisy),  inny: kubernetess

`Ribbon` przyklady do dzialania load balancera

`HystixClient` symulacja faila (np mozna dodac timoeut)

`Zuul` routing + filtry np przy zmianie wersji api (np dodanie /api/v1/ do urla)

## 24.07:
- aspekty `@Aspect` (opieraja sie na AspectJ):  np do cichego logowania lub zmiany obslugi metod do ktorych nie mamy dostepu do zrodel

`@Schedule(1000)` wywoluje sie co przedzial czasu (mozna uzyc crona); w Spring

- konwencja w springu: `schema.sql` zawsze jest wolana i np tworzy schema bazy w data.sql (import.sql) trzymamy np dodanie danych do bazy
- pry profilowanych zrodlach sql: np dwa profile: schema-mysql.sql i schema-postgres.sql
- `redis db` najczesciej uzywana do kolejek lub cache (klucz-wartosc)

- W spring: z entity tworzymy schemat bazy

- najlepiej robic profile do roznych srodowisk:
`sprinf.profiles.active="develop"`
`sprinf.profiles.active="prod"`

- w linii komend mozna uruchamiac profil (w jenkinsie najlepiej dodac wtedy zawsze prod)
- w app.properties: debug:false - niekoniecznie potrzebne

- blog-rest: temat hibernate
- `...hibernate.sql-DEBUG` przydatne do sprawdzania wykonywanych sqli

- FetchType.Eager (hibernate)

**MVC:**
- `FrontController` jest domyslny w spring i nie musimy go "robic"
- `UserController` @PathVariable i @RequestBody
- `UserRepository` @Param

- DTOsy nie robic ;)
- DDD: domain drvien dev
- DAO - czesto stosowane w microserwisach (dao trzyma np id,name i zapisuje do bazy + jakas obsluga biznesowa)

- konfiguracja cors ?

## 23.07:

- `@Configuration` moze byc wiele; jesli zalezy na kolejnosc ladowania trzeba to zrobic np w jendym miejscu
- `@ComponentScan` - najlepiej wlaczyc najwczesniej czyli np w `@Configuration`
- `useDefaultFilters=true` szuka tylko te zagniezdzone a nie te zadeklarowane klasy (?); taki exclude
- Adnotacje - scope
- application -> cykl zycia ServletContext czyli zycie calej aplikacji w spring (?)
- websocket ?
- listnery aplikacji i kontekstu ?

//
`AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);` tak robimy w czystym springu albo `@EnableAutoconfiguration`

`DataSourceJpaConfiguration.class` przyklad dla jpa jesli chcemy podmienic konfiguracje kopiujemy i zmieniamy

**cykl zycia ziarna (kolejnosc):**
1) adnotacje
2) intrefejsy: `InitializaingBean`
3) metody: `init` i `destroy` metody musza byc bezparametrowe i void
