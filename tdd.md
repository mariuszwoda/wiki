# java tdd

## workshop [stacja.it](https://stacja.it/warsztaty/2018-11-17-tdd-java.html), 2018-11-17
Prowadzenie: Dmytro Svarychevskyi

### teoria

tdd: to jak pisac projektowac aplikacje w oparciu o tdd

S.O.L.I.D. STANDS FOR:

S — Single responsibility principle

O — Open closed principle

L — Liskov substitution principle

I — Interface segregation principle

D — Dependency Inversion principle

* unit testy pisza programisci
* integration test pisza testerzy
* testy penetracyjnen pisze np bezpieka

Mamy 3 kroki:
1. piszemy test
2. piszemy implementacje funkcjonalnosci
3. refaktoryzacja kodu (jesli np dobrze napisany kod nie trzeba wiele zmieniac)

tdd ulatwia utrzymanie aplikacji ale utrudnia utrzymanie testow

### praktyka

* projekt 'lodowka'; podzieleni na 7 grup; kazda grupa pisala swoja implementacje funkcjonalnosci w trybie tdd
* projekt koncowy [javatdd](https://github.com/SvDmytro/javatdd)
