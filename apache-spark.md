# apache-spark

## workshop [stacja-it](https://stacja.it/warsztaty/2019-03-16-Apache-Spark-2---Przetwarzanie-Big-Data.html), 2019-03-16
 Wojciech Koszycki 

### czesc teoretyczna
- `Hadoop` sluzy do przetrzymywania danych rozproszonych
- `MapReduce` sluzy do zarzadzania tymi danymi na hadoopie 
- pozniej powstal `Hive` (facebook) za pomoca sqla przetwarzamy dane 

### czesc praktyczna
- `/usr/local/Cellar/sbt/1.2.8` katalog instalacji sbt w macos
- `sbt` to menadzer pakietow dla scali, tak jak maven 
- zadania i rozwiazania sa dostepne na [git](https://github.com/wkoszycki/spark-workshop/tree/solutions/src/main/scala/com/github/koszycki/spark/workshop) w branchu `solutions`

- `com.github.koszycki.spark.workshop.PopularMovies` jesli chcemy aby dane byly na kazdym executorze to mozna  uzyc `sc.broadcast(m)`. Potrzebne gdy chcemy uzywac tych samych danych w kilku operacjach. [jacek-laskowski](https://jaceklaskowski.gitbooks.io/mastering-apache-spark/spark-broadcast.html)

- RDD ?

- `graphx` wtyczna do struktów grafowych

- `com.github.koszycki.spark.workshop.MovieSimilarities` jesli dwoch ludzi obejrzalo te same filmy i trzeci czlowiek ogladal tylko jeden z nich, mozemy znalezc pierwszy film i mu go zarekomendowac

- w konsoli w `ammonite` mozna wykonac wszystkie operacje i sprawdzic w `stage`'ach który RDD został wykonany kilkukrotnie

- `DataFrame.scala` plik w ktorym są komendy do kopiowania do ammonite (nie odpalimy go z poziomu idei).
- `locahost:4040` Spark Jobs na ktorej mozna podpatrzec zapytania instrukcje wykonywane etd. Konsola jest dostepna po odpaleniu sesji sparka w konsoli `ammonite`

- `com.github.koszycki.spark.workshop.PopularHashtags` przyklad dla twittera

- batch przetwarzamy kawalki, a streming dzialaja caly czas, nasluchuja i w momencie kiedy sie pojawia dane przetwarzaja 
- nalezy uzywac StructuredStreaming (czyta caly czas) zamiast Streaming (podzielony na okna czasowe w ktorych czyta)
- https://linkurio.us/product/#sdk